package client.character;

/**
 * Created on 12/20/2017.
 */
public class Quest {
    private int QRKey;
    private String QRValue;

    public int getQRKey() {
        return QRKey;
    }

    public void setQRKey(int QRKey) {
        this.QRKey = QRKey;
    }

    public String getQRValue() {
        return QRValue;
    }

    public void setQRValue(String QRValue) {
        this.QRValue = QRValue;
    }
}
