package client.character.skills;

import util.Position;
import util.Rect;

public class MobAttackInfo {
    public int mobId;
    public byte idk1;
    public byte idk2;
    public byte idk3;
    public byte idk4;
    public byte idk5;
    public byte calcDamageStatIndex;
    public short rcDstX;
    public short rectRight;
    public short oldPosX;
    public short oldPosY;
    public short hpPerc;
    public int[] damages;
    public int mobUpDownYRange;
    public byte type;
    public String currentAnimationName;
    public int animationDeltaL;
    public String[] hitPartRunTimes;
    public int templateID;
    public short idk6;
    public boolean isResWarriorLiftPress;
    public Position pos1;
    public Position pos2;
    public Rect rect;
    public int idkInt;
}
