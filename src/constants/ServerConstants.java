package constants;

/**
 * Created on 2/18/2017.
 */
public class ServerConstants {

    public static final byte LOCALE = 8;
    public static final String WZ_DIR = "D:\\SwordieMS\\Swordie\\WZ";
    public static final String DAT_DIR = "D:\\SwordieMS\\Swordie\\dat";
    public static final int MAX_CHARACTERS = JobConstants.LoginJob.values().length * 3;
    public static short VERSION = 176;
    public static String MINOR_VERSION = "1";
    public static int LOGIN_PORT = 8484;
}
